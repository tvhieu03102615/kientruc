"use strict";
jQuery(document).ready(function($){


  var currentScroll;
  $('.toggle-aside i').click(function(){
     $('#aside').css("left","0");
     $("#click-closing").addClass('activing');
    currentScroll = $(window).scrollTop();
  });

  $('.close').click(function(){
    $('#aside').css("left","-100%");
    $("#click-closing").removeClass('activing');
 });
 $(window).resize(function(){
  if($(window).width()>1024){
    $('#aside').css("left","0");
  }
  else {
    $('#aside').css("left","-100%");
  }
 });
  $(window).scroll(function(){
    var afterScroll = $(window).scrollTop();
    if($(window).width()<=1024){
      if(afterScroll != currentScroll){
        $("#aside").css("left","-100%");
        $("#click-closing").removeClass('activing');
      }
    }
  });
  $("#click-closing").click(function(event) {
    $("#aside").css("left","-100%");
    $("#click-closing").removeClass('activing');
  });
  $('.banner .owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    items: 1
  });
  $('.slide-design').owlCarousel({
    onChanged:callback,
    loop:true,
    margin:10,
    responsive:{
        0:{
          center: false,
          items:1
        },
        600:{
          center: true,
          items:2
        }
    }
  });

  $('.reference-design .owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    items: 1
  });

  $(window).resize(function(event) {
    if($(window).width() > 1024) {
      $("#aside").show();
    }
  });

  var owl = $(".slide-design");
  $(".next").click(function(){
    owl.trigger('next.owl.carousel');
  })
  $(".prev").click(function(){
    owl.trigger('prev.owl.carousel');
  })

  function callback(event) {
    var items     = event.item.count;     // Number of items
    var page      = event.page.index;     // Position of the current page
    page++;
     if(page == 0){
       page=1;
       
     }
    $('.count span:nth-child(3)').text(items);
    $('.count span:nth-child(1)').text(page);
  }
  $.validator.addMethod("validateName", function (value, element) {
      return this.optional(element) || /^[a-zA-Z ]*$/i.test(value);
  }, "*Không được nhập số ở đây!");
$('#form').validate({
  rules:{
    name:{
      required:true,
      validateName: true,
    },
    phone:{
      required:true,
      number:true,
      minlength:10,
      maxlength:11,
    },
    message:{
      required:true,
    }
  },
  messages:{
    name:{
      required:"*Bạn chưa nhập dữ liệu",
      number: "*Không được nhập số ở đây",
      validateName: "*Không được nhập số ở đây!",
    },
    phone:{
      required:"*Bạn chưa nhập dữ liệu",
<<<<<<< HEAD
      number:"*Vui lòng nhập số hợp lệ",
      minlength: jQuery.validator.format("*Phải nhập ít nhất 10 kí tự"),
      maxlength: jQuery.validator.format("*nhập lớn nhất 11 kí tự"),
=======
      number:"* Vui lòng nhập số hợp lệ",
      minlength: jQuery.validator.format("* Nhập ít nhất 10 kí tự"),
      maxlength: jQuery.validator.format("* Nhập lớn nhất 11 kí tự"),
>>>>>>> e9736939b39ff25aed15c9ff2d80765012b99371
    },
    message:{
      required:"*Bạn chưa nhập dữ liệu",
    },
  }
  
});

});
